﻿#include <iostream>

void EvenOrNaturalNumbers(int N, bool IsEven) {
    switch (IsEven) {
    case true: {
        for (int i = 0; i <= N; i++) {
            if (i % 2 == 0) {
                std::cout << i << '\n';
            }
        };
        break;
    }
    case false: {
        for (int i = 0; i <= N; i++) {
            if (i % 2 != 0) {
                std::cout << i << '\n';
            }
        }
    }
    }
}


int main()
{
    const int N = 20;
    bool IsEven = false;
    EvenOrNaturalNumbers(N, IsEven);
}
